package main

import (
	// "time"
)

type ShortLink struct {
	RootLink   string
	ShortLink  string
	Created    string
	Accesses   int64
	LastAccess string
}